<?php declare(strict_types=1);

namespace Hackathon\Command;

use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

final class DhcpCommand extends HackathonBaseCommand
{
    protected function configure()
    {
        $this->setname('updater');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $this->container['dhcp_service']->refresh();
    }
}
