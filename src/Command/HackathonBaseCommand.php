<?php declare(strict_types=1);

namespace Hackathon\Command;

use Pimple\Container;
use Symfony\Component\Console\Command\Command;

/**
 * Class HackathonBaseCommand
 *
 * @package Hackathon\Commands
 */
class HackathonBaseCommand extends Command
{
    /** @var Container */
    protected $container;

    /**
     * HackathonCommand constructor.
     *
     * @param string    $name
     * @param Container $container
     */
    public function __construct($name, Container $container)
    {
        $this->container = $container;
        parent::__construct($name);
    }
}
