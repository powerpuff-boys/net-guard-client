<?php declare(strict_types=1);

namespace Hackathon\Command;

use Hackathon\Utils\Config;
use Hackathon\Utils\File;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

final class DeployCommand extends HackathonBaseCommand
{
    protected function configure()
    {
        $this->setname('deploy');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $output->writeln('Copying firewall file.');
        $this->copyFirewall();

        $output->writeln('Copying periodic file.');
        $this->copyPeriodic();

        $output->writeln('Copying html files.');
        $this->copyHtml();

        $output->writeln('Copying httpd files.');
        $this->copyHttpd();
        
        exec('systemctl reload httpd.service');
    }

    private function copyFirewall()
    {
        $firewallContents = file_get_contents(Config::getFirewallPath());

        $firewallContents = str_replace(
            ['{{ internal_interface }}', '{{ external_interface }}'],
            [Config::getInternalInterface(), Config::getExternalInterface()],
            $firewallContents
        );

        File::write('/etc/rc.d/' . basename(Config::getFirewallPath()), $firewallContents, 0755);
    }

    private function copyPeriodic()
    {
        copy(Config::getPeriodicFilePath(), '/etc/rc.d/' . basename(Config::getPeriodicFilePath()));
    }

    private function copyHtml()
    {
        File::copy(RESOURCES_DIR . '/html', '/var/www/html');
    }

    private function copyHttpd()
    {
        File::copy(RESOURCES_DIR . '/httpd', '/etc/httpd/conf.d');
    }
}
