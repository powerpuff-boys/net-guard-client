<?php declare(strict_types=1);

namespace Hackathon\Command;

use Hackathon\Events\ShutdownEvent;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\ArrayInput;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

final class RunCommand extends HackathonBaseCommand
{
    protected function configure()
    {
        $this->setname('run');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $commands = [
            $this->getApplication()->find('bandwidth'),
            $this->getApplication()->find('updater'),
        ];

        $output->writeln('Starting up ...');
        /** @var Command $command */
        foreach ($commands as $command) {
            $input = new ArrayInput([]); # options for command to be run
            $returnCode = $command->run($input, $output);
            if (0 === $returnCode) {
                $output->writeln('Ran command ' . $command->getName() . '.');
            }
        }

        $output->writeln('Executing pre-shutdown actions.');
        $this->container['event_dispatcher']->dispatch(ShutdownEvent::NAME, new ShutdownEvent());

        $output->writeln('Shutting down.');
    }
}
