<?php declare(strict_types=1);

namespace Hackathon\EventSubscribers;

use Hackathon\Events\ShutdownEvent;
use Hackathon\Utils\Config;
use Hackathon\Utils\File;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

final class Subscriber implements EventSubscriberInterface
{
    public static function getSubscribedEvents()
    {
        return [
            ShutdownEvent::NAME => 'onShutdown',
        ];
    }

    public function onShutdown(ShutdownEvent $event)
    {

    }
}
