<?php declare(strict_types=1);

namespace Hackathon\Events;

use Symfony\Component\EventDispatcher\Event;

final class ShutdownEvent extends Event
{
    const NAME = 'hackathon_client.on_shutdown';

    # empty for now
}
