<?php declare(strict_types=1);

namespace Hackathon\Repository;

use Hackathon\Utils\Config;

final class IpRepository extends AbstractRepository
{
    public function getAll()
    {
        $sql = '
            SELECT
              INET_NTOA(ipa.ipv4) as ip,
              ipa.mac as mac,
              ipa.status as status,
              d.id as device_id,
              d.name as device_name
            FROM ip_addresses = ipa
            LEFT JOIN devices d ON ipa.device_id = d.id
            WHERE ipa.pop_id = :pop_id
        ';

        $sth = $this->connection->prepare($sql);
        $sth->execute([':pop_id' => Config::getPopId()]);

        return $sth->fetchAll();
    }

    public function getForDevice(int $deviceId)
    {
        $sql = '
            SELECT
              INET_NTOA(ipa.ipv4) as ip,
              ipa.mac as mac
            FROM ip_addresses ipa
            WHERE ipa.status = 1 AND ipa.device_id = :device_id AND pop_id = :pop_id
        ';

        $sth = $this->connection->prepare($sql);
        $sth->execute([
            ':device_id' => $deviceId,
            ':pop_id'    => Config::getPopId(),
        ]);

        return $sth->fetchAll();
    }
}
