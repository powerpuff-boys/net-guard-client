<?php declare(strict_types=1);

namespace Hackathon\Repository;

abstract class AbstractRepository
{
    protected $connection;

    public function __construct($connection)
    {
        $this->connection = $connection;
    }
}
