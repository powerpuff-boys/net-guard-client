<?php declare(strict_types=1);

namespace Hackathon\Repository;

use Hackathon\Utils\Config;

final class DeviceRepository extends AbstractRepository
{
    public function getDevices()
    {
        $sql = '
            SELECT
              d.id as id,
              d.name as name,
              b.value as bandwidth
            FROM devices d
            INNER JOIN bandwidths b ON b.id = d.bandwidth_id
            INNER JOIN ip_addresses ipa ON ipa.device_id = d.id
            WHERE d.status = 1 AND ipa.pop_id = :pop_id
            GROUP BY d.id
        ';

        $sth = $this->connection->prepare($sql);
        $sth->execute([':pop_id' => Config::getPopId()]);

        return $sth->fetchAll();
    }
}
