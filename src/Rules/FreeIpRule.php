<?php declare(strict_types=1);

namespace Hackathon\Rules;

final class FreeIpRule implements RuleInterface
{
    private $ip;

    public function __construct(string $ip)
    {
        $this->ip = $ip;
    }

    public function __toString(): string
    {
        return 'iptables -A FORWARD -p all -s ' . $this->ip . " -d 0/0 -j DROP\n";
    }
}
