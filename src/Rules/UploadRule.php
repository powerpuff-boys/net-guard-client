<?php declare(strict_types=1);

namespace Hackathon\Rules;

use Hackathon\Utils\Config;

final class UploadRule implements RuleInterface
{
    private $deviceId;
    private $bandwidth;

    public function __construct($deviceId, $bandwidth)
    {
        $this->deviceId = $deviceId;
        $this->bandwidth = $bandwidth;
    }

    public function __toString(): string
    {
        return 'RATE='. Config::getMinBandwith() . "kbit\nCEIL=" . $this->bandwidth . "kbit\nMARK=" . $this->deviceId . "\nPRIO=5\n";
    }
}
