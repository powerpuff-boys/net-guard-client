<?php declare(strict_types=1);

namespace Hackathon\Rules;

use Hackathon\Utils\Config;

final class DownloadRule implements RuleInterface
{
    private $ips;
    private $bandwidth;

    public function __construct(array $ips, $bandwidth)
    {
        $this->ips = $ips;
        $this->bandwidth = $bandwidth;
    }

    public function __toString(): string
    {
        $output = 'RATE=' . Config::getMinBandwith() . "kbit\nCEIL=" . $this->bandwidth . "kbit\nPRIO=5\n";
        foreach ($this->ips as $ipAddress) {
            $output .= 'RULE=' . $ipAddress['ip'] . "\n";
        }

        return $output;
    }
}
