<?php declare(strict_types=1);

namespace Hackathon\Rules;

final class BandwidthRule implements RuleInterface
{
    private $deviceId;
    private $ips;

    public function __construct($deviceId, array $ips)
    {
        $this->deviceId = $deviceId;
        $this->ips = $ips;
    }

    public function __toString(): string
    {
        $output = '';
        foreach ($this->ips as $ipAddress) {
            $output .= 'iptables -A POSTROUTING -t mangle -p all -s ' . $ipAddress['ip'] . ' -j MARK --set-mark ' . $this->deviceId . "\n";
        }

        return $output;
    }
}
