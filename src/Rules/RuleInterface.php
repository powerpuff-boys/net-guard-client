<?php declare(strict_types=1);

namespace Hackathon\Rules;

interface RuleInterface
{
    public function __toString(): string;
}
