<?php declare(strict_types=1);

namespace Hackathon\Rules;

use Hackathon\Utils\Config;

final class SuspendedRule implements RuleInterface
{
    private $ip;
    private $deviceName;
    private $serverInternalIp;

    public function __construct(string $ip, string $deviceName)
    {
        $this->ip = $ip;
        $this->deviceName = $deviceName;
        $this->serverInternalIp = Config::getServerInternalIp();
    }

    public function __toString(): string
    {
        return "#{$this->deviceName} cu IP "
        . $this->ip . "\niptables -t nat -I PREROUTING -p tcp --dport 80 -s "
        . $this->ip . " -d 0/0 -j DNAT --to {$this->serverInternalIp}:81\niptables -A FORWARD -p all -s "
        . $this->ip . " -d 0/0 -j DROP\n\n";
    }
}
