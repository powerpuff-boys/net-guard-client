<?php declare(strict_types=1);

namespace Hackathon\Rules;

final class DhcpRule implements RuleInterface
{
    private $ip;
    private $mac;
    private $hostname;

    public function __construct(string $ip, $mac, string $hostname)
    {
        $this->ip = $ip;
        $this->mac = $mac;
        $this->hostname = $hostname;
    }

    public function __toString(): string
    {
        if (!$this->mac) {
            return '';
        }

        $output = <<<STR
host {$this->hostname}_{$this->ip} {
    hardware ethernet {$this->mac};
    fixed-address {$this->ip};
}\n
STR;

        return $output;
    }
}
