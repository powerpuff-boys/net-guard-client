<?php declare(strict_types=1);

namespace Hackathon\Service;

use Hackathon\Repository\IpRepository;
use Hackathon\Rules\DhcpRule;
use Hackathon\Rules\FreeIpRule;
use Hackathon\Rules\SuspendedRule;
use Hackathon\Utils\Config;
use Hackathon\Utils\File;

final class DhcpService
{
    /** @var IpRepository */
    private $ipRepository;

    public function __construct(IpRepository $ipRepository)
    {
        $this->ipRepository = $ipRepository;
    }

    public function refresh()
    {
        $freeIps = '';
        $suspended = '';
        $dhcpContent = file_get_contents(Config::getDhcpTemplatePath());
        $macs = '';

        foreach ($this->ipRepository->getAll() as $ipAddress) {
            if (!$ipAddress['device_id']) {
                $freeIps .= (string)(new FreeIpRule($ipAddress['ip']));
                continue;
            }

            if (0 === (int)$ipAddress['status']) {
                $suspended .= (string)(new SuspendedRule($ipAddress['ip'], $ipAddress['device_name']));
            }

            $hostName = trim(str_replace([' ', '.'], '_', trim($ipAddress['device_name']) . $ipAddress['device_id']), '_');

            $macs .= '# ' . $hostName . "\narp -s " . $ipAddress['ip'] . "\t" . $ipAddress['mac'] . "\n";
            $dhcpContent .= (string)(new DhcpRule($ipAddress['ip'], $ipAddress['mac'], $hostName));
        }

        File::write('/etc/rc.d/rc.arp', $macs, 0755);
        File::write('/etc/dhcp/dhcpd.conf', $dhcpContent);
        File::write('/etc/rc.d/rc.free', $freeIps, 0755);
        File::write('/etc/rc.d/rc.suspended', $suspended, 0755);
    }
}
