<?php declare(strict_types = 1);

namespace Hackathon\Service;

use Hackathon\Repository\DeviceRepository;
use Hackathon\Repository\IpRepository;
use Hackathon\Rules\BandwidthRule;
use Hackathon\Rules\DownloadRule;
use Hackathon\Rules\UploadRule;
use Hackathon\Utils\Config;
use Hackathon\Utils\File;

final class BandwithService
{
    /** @var DeviceRepository */
    private $deviceRepository;

    /** @var IpRepository */
    private $ipRepository;

    public function __construct(DeviceRepository $deviceRepository, IpRepository $ipRepository)
    {
        $this->deviceRepository = $deviceRepository;
        $this->ipRepository = $ipRepository;
        $this->bandwithRuleLocation = Config::getBandwithRuleLocation();
    }

    public function refresh()
    {
        $this->clearExistingRules();
        $this->buildNewRules();
    }

    private function clearExistingRules()
    {
        foreach (glob($this->bandwithRuleLocation . '/en*-2:*') as $filename) {
            unlink($filename);
        }
    }

    private function buildNewRules()
    {
        $ipTablesContent = '';

        /** @var array $devices */
        $devices = $this->deviceRepository->getDevices();
        foreach ($devices as $device) {
            $deviceId = $device['id'] + 10;
            $deviceIps = $this->ipRepository->getForDevice((int)$device['id']);

            # rules for download
            $downloadRulesFile = $this->bandwithRuleLocation . '/' . Config::getInternalInterface() . '-2:' . $deviceId;
            $downloadRulesContent = (string)(new DownloadRule($deviceIps, $device['bandwidth']));

            # rules for upload
            $uploadRulesFile = $this->bandwithRuleLocation . '/' .  Config::getExternalInterface() . '-2:' . $deviceId;
            $uploadRulesContent = (string)(new UploadRule($deviceId, $device['bandwidth']));

            # update ip tables
            $ipTablesContent .= new BandwidthRule($deviceId, $deviceIps);

            File::write($downloadRulesFile, $downloadRulesContent);
            File::write($uploadRulesFile, $uploadRulesContent);
        }

        File::write('/etc/rc.d/rc.bandwidth', $ipTablesContent, 0755);
    }
}
