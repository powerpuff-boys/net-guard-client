<?php declare(strict_types=1);

namespace Hackathon\Utils;

final class Config
{
    private static $instance;

    /** @var array */
    private $config;

    protected function __construct()
    {
        $this->config = parse_ini_file(BASE_DIR . '/resources/config.ini');
        if (false === $this->config) {
            throw new \Exception('Error occured while parsing config.ini file.');
        }
    }

    protected static function getInstance(): Config
    {
        if (null === static::$instance) {
            static::$instance = new static();
        }

        return static::$instance;
    }

    private function getKey(string $key)
    {
        if (!array_key_exists($key, $this->config)) {
            throw new \Exception("Config key \"$key\" not found.");
        }

        return $this->config[$key];
    }

    public static function getPopId(): int
    {
        return (int)(self::getInstance())->getKey('pop_id');
    }

    public static function getDatabaseHost(): string
    {
        return (self::getInstance())->getKey('db_host');
    }

    public static function getDatabaseUsername(): string
    {
        return (self::getInstance())->getKey('db_username');
    }

    public static function getDatabasePassword(): string
    {
        return (self::getInstance())->getKey('db_password');
    }

    public static function getDatabaseName(): string
    {
        return (self::getInstance())->getKey('db_name');
    }

    public static function getMinBandwith(): int
    {
        return (int)(self::getInstance())->getKey('min_bandwith');
    }

    public static function getBandwithRuleLocation(): string
    {
        return (self::getInstance())->getKey('bandwidth_rule_location');
    }

    public static function getServerInternalIp(): string
    {
        return (self::getInstance())->getKey('server_internal_ip');
    }

    public static function getDhcpTemplatePath(): string
    {
        return RESOURCES_DIR . '/dhcp.conf.base';
    }

    public static function getFirewallPath(): string
    {
        return RESOURCES_DIR . '/rc.firewall';
    }

    public static function getPeriodicFilePath(): string
    {
        return RESOURCES_DIR . '/rc.periodic';
    }

    public static function getExternalInterface(): string
    {
        return (self::getInstance())->getKey('external_interface');
    }

    public static function getInternalInterface(): string
    {
        return (self::getInstance())->getKey('internal_interface');
    }
}
