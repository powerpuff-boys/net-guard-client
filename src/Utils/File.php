<?php declare(strict_types = 1);

namespace Hackathon\Utils;

final class File
{
    public static function write(string $name, string $contents, $chmod = null, string $mode = 'w')
    {
        if (file_exists($name) && !is_writable($name)) {
            throw new \Exception("The file $name already is not writable");
        }

        if (!$handle = fopen($name, $mode)) {
            throw new \Exception("Cannot open file ($name)");
        }

        if (false === fwrite($handle, $contents)) {
            throw new \Exception("Cannot write to file ($name)");
        }

        if ($chmod) {
            chmod($name, $chmod);
        }

        fclose($handle);
    }

    public static function copy($source, $dest)
    {
        shell_exec(escapeshellcmd('cp -a ' . $source . '/. ' . $dest . '/'));
    }
}
