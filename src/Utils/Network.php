<?php declare(strict_types=1);

namespace Hackathon\Utils;

final class Network
{

    public function isMAC($address): bool
    {
        if($address == "") return 1;
        if(strlen($address) > 17) return 0;
        $address=strtoupper($address);
        if (!eregi("^[0-9A-F]+(\:[0-9A-F]+)+(\:[0-9A-F]+)+(\:[0-9A-F]+)+(\:[0-9A-F]+)+(\:[0-9A-F]+)$",$address)) return 0;
        $array=explode(":",$address);
        if(strlen($array[0]) != 2) return 0;
        if(strlen($array[1]) != 2) return 0;
        if(strlen($array[2]) != 2) return 0;
        if(strlen($array[3]) != 2) return 0;
        if(strlen($array[4]) != 2) return 0;
        if(strlen($array[5]) != 2) return 0;
        return 1;
    }

    public function isuniqueMAC ($address): string
    {
        global $ip_table;
        //daca e gol il consideram valid
        if ($address=="") return 1;
        $unic = 1;
        $query = "select * from `".$ip_table."` where `mac`='".$address."';";
        $result = mysql_query ($query);
        if (mysql_num_rows ($result)>0) $unic = 0;
        return $unic;
    }


    public function isCorrectDns($dns): bool
    {
        if ($dns == "") return 1;
        if (!strpos($dns, ".ktvnet.ro")) return 0;
        return 1;
    }

    public function isIp ($ip): bool
    {
        $array=explode(".",$ip);
        if(($array[0]>254)||($array[0]<1)) return 0;
        if(($array[1]>254)||($array[1]<1)) return 0;
        if(($array[2]>254)||($array[2]<1)) return 0;
        if(($array[3]>254)||($array[3]<1)) return 0;
        return 1;
    }
}
