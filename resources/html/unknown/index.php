<?php 
$mac = exec('arp -n ' . $_SERVER['REMOTE_ADDR'] . ' | grep -v Flags | awk \'{printf $3}\'');
?><html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>Suspended connection</title>
</head>
<body style="background: url(bgd.jpg) no-repeat center center fixed; background-size: cover; color: #666;">
    <div style="margin:100px auto 0px auto; width: 800px;text-align:center; text-shadow: 2px 2px #ddd; opacity: 0.9; background-color: #fff; padding: 10px; border-radius: 6px;">
	<h1>YOUR DEVICE IS NOT REGISTERED IN OUR SYSTEM</h1>
	<h3>Please contact us at 07xxxxxxxx or 031yyyyyy
	in order to gain external access!</h3>
	<h3>
	    Your IP address: <?php echo $_SERVER["REMOTE_ADDR"] ?>
	    and your MAC address is <?php echo $mac; ?>
	</h3>
    </div>
</body>
</html>