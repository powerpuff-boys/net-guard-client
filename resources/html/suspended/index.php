<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>Suspended connection</title>
</head>
<body style="background: url(bgd.jpg) no-repeat center center fixed; background-size: cover; color: #FFF;">
    <div style="margin:100px auto 0px auto; width: 800px;text-align:center; text-shadow: 3px 3px #333">
	<h1>YOUR CONNECTION IS SUSPENDED</h1>
	<h3>Please contact the network administrator at<br />
	 07xxxxxxxx or 031yyyyyy, in order to resolve the situation</h3>
	<h3>
	    Current IP address: <?php echo $_SERVER["REMOTE_ADDR"] ?>
	</h3>
    </div>
</body>
</html>